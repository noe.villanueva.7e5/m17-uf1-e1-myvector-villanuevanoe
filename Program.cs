using System;
using System.Collections.Generic;

namespace M17_UF1_E1_MyVector_VillanuevaNoe
{
    class Program
    {
        static void Main()
        {
            Exercici1();
            Exercici2();
            Exercici3();
        }

        public static void Exercici1()
        {
            Console.WriteLine("Introdueix nom: ");
            var nom = Console.ReadLine();
            Console.WriteLine("Hello " + nom + "! \n");

        }

        public static void Exercici2()
        {
            var vector1 = new MyVector(new double[] { 1, 1 }, new double[] { 4, 5 });
            Console.WriteLine(vector1.ToString());

            var vector2 = new MyVector(new double[] { 10, 5 }, new double[] { 3, 10 });
            Console.WriteLine(vector2.ToString());
            vector2.SetIniciA(1);
            vector2.SetFinalA(20);
            vector2.SetIniciB(8);
            vector2.SetFinalB(5);
            Console.WriteLine(vector2.ToString());
            vector2.CanviSentit();
            Console.WriteLine(vector2.ToString());
            Console.WriteLine("\n");
        }

        public static void Exercici3()
        {
            var vectorG = new VectorGame();
            var array1 = vectorG.randomVectors(5);
            var ordenada = vectorG.SortVectors(array1, true);
            for (int i = 0; i < ordenada.Length; i++)
            {
                Console.WriteLine(ordenada[i].ToString());
            }
            vectorG.ShowVectorDirection(ordenada);
        }

        public class MyVector
        {
            private double[] _puntA;
            private double[] _puntB;

            public MyVector(double[] puntA, double[] puntB)
            {
                this._puntA = puntA;
                this._puntB = puntB;
            }

            public void SetIniciA(double iniciA)
            {
                _puntA[0] = iniciA;
            }
            public void SetFinalA(double finalA)
            {
                _puntA[1] = finalA;
            }
            public void SetIniciB(double iniciB)
            {
                _puntB[0] = iniciB;
            }
            public void SetFinalB(double finalB)
            {
                _puntB[1] = finalB;
            }

            public double GetIniciA()
            {
                return _puntA[0];
            }
            public double GetIniciB()
            {
                return _puntB[0];
            }
            public double GetFinalA()
            {
                return _puntA[1];
            }
            public double GetFinalB()
            {
                return _puntB[1];
            }

            public double Distancia()
            {
                double distancia = Math.Sqrt(Math.Pow(_puntB[0] - _puntA[0], 2) + Math.Pow(_puntB[1] - _puntA[1], 2));
                return distancia;
            }

            public void CanviSentit()
            {
                var aux = _puntA;
                _puntA = _puntB;
                _puntB = aux;
            }
            public override string ToString()
            {
                return "Vector : [ " + (_puntB[0] - _puntA[0]) + " , " + (_puntB[1] - _puntA[1]) + " ] amb distancia " + Distancia();
            }
        }

        public class VectorGame
        {
            public MyVector[] randomVectors(int llargada)
            {
                var arrayVectors = new List<MyVector>();
                Random randNum = new Random();
                for (int i = 0; i < llargada; i++)
                {
                    var vector = new MyVector(new double[] { randNum.Next(-20, 20), randNum.Next(-20, 20) }, new double[] { randNum.Next(-20, 20), randNum.Next(-20, 20) });
                    arrayVectors.Add(vector);
                }
                return arrayVectors.ToArray();
            }

            public MyVector[] SortVectors(MyVector[] array, bool ordre)
            {
                if (ordre)
                {
                    MyVector t;
                    for (int a = 1; a < array.Length; a++)
                        for (int b = array.Length - 1; b >= a; b--)
                        {
                            if (array[b - 1].Distancia() > array[b].Distancia())
                            {
                                
                                t = array[b - 1];
                                array[b - 1] = array[b];
                                array[b] = t;
                            }
                        }
                }

                return array;
            }

            public void ShowVectorDirection(MyVector[] arrayVectors)
            {
                foreach (MyVector elem in arrayVectors)
                {
                    if (elem.GetIniciB() - elem.GetIniciA() > 0  && elem.GetFinalB() - elem.GetFinalA() > 0)
                    {
                        Console.Write("La direcció del " + elem + " és : ");
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("/  (1er quadrant)\n");
                    }else if (elem.GetIniciB() - elem.GetIniciA() > 0 && elem.GetFinalB() - elem.GetFinalA() < 0)
                    {
                        Console.Write("La direcció del " + elem + " és : ");
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.Write("\\ (4rt quadrant)\n");
                    }else if (elem.GetIniciB() - elem.GetIniciA() < 0 && elem.GetFinalB() - elem.GetFinalA() > 0)
                    {
                        Console.Write("La direcció del " + elem + " és : ");
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.Write("\\  (2on quadrant)\n");
                    }else
                    { 
                        Console.Write("La direcció del " + elem + " és : ");
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("/  (3er quadrant)\n");
                    }
                    Console.ResetColor();
                }
            }
        }
    }
}
